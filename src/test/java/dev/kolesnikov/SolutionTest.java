package dev.kolesnikov;

import org.junit.jupiter.api.Test;

import java.util.List;

import static java.lang.Math.pow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SolutionTest {

    @Test
    public void testCase1() {
        List<Integer> centers = List.of(-2, 1, 0);
        long d = 8;

        Solution solution = new Solution();
        Integer result = solution.solve(centers, d);

        assertEquals(3, result);
    }

    @Test
    public void testCase2() {
        List<Integer> centers = List.of(-1004, -4, 996);
        long d = 4002;

        Solution solution = new Solution();
        Integer result = solution.solve(centers, d);

        assertEquals(3, result);
    }

    @Test
    public void testCase3() {
        List<Integer> centers = List.of(-1000, -1001, -1002, -1003, -1004, -1005, -1006, -1007, -1008, -1009, 1000);
        long d = 4060;

        Solution solution = new Solution();
        Integer result = solution.solve(centers, d);

        assertEquals(3, result);
    }

    @Test
    public void testCase4() {
        List<Integer> centers = List.of((int) -pow(10, 9), (int) pow(10, 9));
        long d = (long) pow(10, 15);

        Solution solution = new Solution();
        Integer result = solution.solve(centers, d);

        // The world is represented by a line of numbers -10^9 to 10^9.
        assertEquals((int) pow(10, 9) * 2 + 1, result);
    }
}
