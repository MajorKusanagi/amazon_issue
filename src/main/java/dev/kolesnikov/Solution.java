package dev.kolesnikov;

import java.util.List;
import java.util.function.Function;

import static java.lang.Math.pow;

/**
 * Complexity = O(n*log m)
 * <br>n - centers.size()
 * <br>m - max left-right difference
 */
public class Solution {

    record LookupResult(int lookup, boolean isDistanceOk, boolean isEdge) {}

    private static final int MIN_VALUE = (int) -pow(10, 9);
    private static final int MAX_VALUE = (int) pow(10, 9);

    public int solve(List<Integer> centers, long d) {
        // Find first
        Integer first = binarySearch(
                centers,
                d,
                MIN_VALUE,
                MAX_VALUE,
                lookupResult -> lookupResult.isDistanceOk,
                lookupResult -> lookupResult.lookup < 0
        );

        // Check at least one position exists
        if (first == null) return 0;

        // Binary search left edge
        Integer leftEdge = binarySearch(
                centers,
                d,
                MIN_VALUE,
                first,
                lookupResult -> lookupResult.isEdge,
                lookupResult -> lookupResult.isDistanceOk
        );

        // Binary search right edge
        Integer rightEdge = binarySearch(
                centers,
                d,
                first,
                MAX_VALUE,
                lookupResult -> lookupResult.isEdge,
                lookupResult -> !lookupResult.isDistanceOk
        );

        // Calculate result
        if (leftEdge == null || rightEdge == null) {
            throw new RuntimeException("leftEdge and rightEdge shouldn't be null!");
        }
        return rightEdge - leftEdge + 1;
    }

    private Integer binarySearch(List<Integer> centers, long d, int left, int right, Function<LookupResult, Boolean> exitCondition, Function<LookupResult, Boolean> goLeftCondition) {
        while (left <= right) {
            int mid = left + (right - left) / 2;
            LookupResult lookupResult = lookup(centers, d, mid);
            if (exitCondition.apply(lookupResult)) {
                return mid;
            }
            if (goLeftCondition.apply(lookupResult))
                right = mid - 1;
            else
                left = mid + 1;
        }
        return null;
    }

    private LookupResult lookup(List<Integer> centers, long d, int pos) {
        int posLeft = pos - 1;
        int posRight = pos + 1;
        long sum = 0;
        long sumLeft = 0;
        long sumRight = 0;
        for (Integer current : centers) {
            sum += (current > pos)
                    ? (current - pos) * 2L
                    : (pos - current) * 2L;
            sumLeft += (current > posLeft)
                    ? (current - posLeft) * 2L
                    : (posLeft - current) * 2L;
            sumRight += (current > posRight)
                    ? (current - posRight) * 2L
                    : (posRight - current) * 2L;
            if (sum > d) {
                return new LookupResult(
                        Long.compare(sumLeft, sumRight),
                        false,
                        false
                );
            }
        }
        return new LookupResult(
                Long.compare(sumLeft, sumRight),
                sum <= d,
                sumLeft > d || sumRight > d || posLeft < MIN_VALUE || posRight > MAX_VALUE
        );
    }
}
